const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const pool = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()

router.post('/user/signup', (request, response) => {
    const { name, gender, phone_no, city, email, password } = request.body
    const encryptedPassword = String(cryptoJS.MD5(password))

    const sql = `INSERT INTO customer(name,gender,phone_no,city,email,password)
                    values(?,?,?,?,?,?)`

    db.pool.query(
        sql,
        [name, gender, phone_no, city, email, encryptedPassword],
        (error, user) => {
            response.send(utils.createResult(error, user))
        }
    )

})

router.post('/user/signin', (request, response) => {
    const { email, password } = request.body
    const encryptedPassword = String(cryptoJS.MD5(password))

    const sql = `SELECT * FROM customer
                WHERE email = ? and password = ?`

    db.pool.query(
        sql,
        [email, encryptedPassword],
        (error, users) => {
            const result = {}

            if (error) {
                result.status = "error"
                result.status = error
            }
            else {
                if (users.length === 0) {
                    result.status = "error"
                    result.error = "user does not exist"
                }
                else {
                    const currentUser = users[0]
                    result.status = "success"

                    const token = jwt.sign({
                        cid: currentUser.cid
                    },
                        config.secret
                    )
                    result.data = {
                        name: currentUser.name,
                        gender: currentUser.gender,
                        phone_no: currentUser.phone_no,
                        city: currentUser.city,
                        email: currentUser.email,
                        token
                    }
                }
            }
            response.send(result)
        }
    )
})

module.exports = router

