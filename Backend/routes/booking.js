const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const pool = require('../db')
const utils = require('../utils')
const { JsonWebTokenError } = require('jsonwebtoken')
const config = require('../config')

const router=express.Router()
router.post('/booking/add', (request, response) => {
  
    const { booking_date, booking_time,service_id } = request.body
      

    const sql = `Insert into booking
                    (booking_date, booking_time,cid,service_id)
                    values(?,?,?,?);`
    
    db.pool.query(
        sql,
        [booking_date, booking_time,request.cid,service_id],
        (error, data) => {
            response.send(utils.createResult(error, data))
           
        })

})

module.exports=router;