const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const pool = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()

router.post('/admin/signup', (request, response) => {

    const { name, gender, email, password } = request.body

    const encryptedPassword = String(cryptoJS.MD5(password))

    const sql = `Insert into admin
                    (name,gender,email,password)
                    values(?,?,?,?);`

    db.pool.query(
        sql,
        [name, gender, email, encryptedPassword],
        (error, data) => {

            response.send(utils.createResult(error, data))
        })

})

router.post('/admin/signin', (request, response) => {

    const { email, password } = request.body
    const encryptedPassword = String(cryptoJS.MD5(password))

    const sql = `select * from admin
                where email = ? and password = ?`

    db.pool.query(
        sql,
        [email, encryptedPassword],
        (error, admin) => {
            const result = {}

            if (error) {
                result.status = "error"
                result.error = error
            }
            else {
                if (admin.length === 0) {
                    result.status = 'error'
                    result.error = 'user does not exist'
                }
                else {
                    const currentAdmin = admin[0]
                    result.status = "success"

                    const token = jwt.sign(
                        {
                            adminId: admin.id
                        },
                        config.secret
                    )

                    result.data = {
                        name: currentAdmin.name,
                        gender: currentAdmin.gender,
                        email: currentAdmin.email,
                        token
                    }
                }
            }
            response.send(result)
        }
    )
})


module.exports = router;