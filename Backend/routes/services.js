const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const pool = require('../db')
const utils = require('../utils')
const multer = require('multer')
const upload = multer({dest : 'storage/'})


//const { JsonWebTokenError } = require('jsonwebtoken')

const router = express.Router()

router.post('/services/add', (request, response) => {

    const { service_name, duration,price,image } = request.body
    


    const sql = `Insert into services
                    (service_name, duration,price,image)
                    values(?,?,?,?);`
    
    db.pool.query(
        sql,
        [service_name, duration,price,image],
        (error, data) => {
            response.send(utils.createResult(error, data))
           
        })

})

router.get('/services', (request, response) => {

    const sql = `select service_id,service_name, duration,price ,image from services;`
    
    db.pool.query( sql,(error, data) => {
            response.send(utils.createResult(error, data))
           
        })

})

router.delete('/services/delete', (request, response) => {
     const {service_id}=request.body

    const sql = `Delete from services where service_id=?;`
    
    db.pool.query( sql,[service_id],(error, data) => {
            response.send(utils.createResult(error, data))
           
        })

})

router.put('/services/update', (request, response) => {
   // const {service_id}=request.params
    const {service_id,service_name, duration,price}=request.body

   const sql = `UPDATE services SET service_name=?,duration=?,price=?
                     where service_id=?;`
   
   db.pool.query( sql,[service_name, duration,price,service_id],(error, data) => {
           response.send(utils.createResult(error, data))
          
       })

})

module.exports = router;