const express = require('express')
const utils = require('./utils')
const cors=require('cors')
const jwt = require('jsonwebtoken')
const config = require('./config')
const { request, response } = require('express')
const { JsonWebTokenError } = require('jsonwebtoken')


const app = express()
app.use(express.json())
app.use(cors())
app.use(express.static('uploads'))
app.use((request, response, next) => {
    if (request.url === '/admin/signup' || request.url === '/admin/signin' || request.url === '/user/signup'
     || request.url === '/user/signin' || request.url === '/services/add' || request.url === '/services'
     || request.url === '/services/delete' || request.url === '/services/update' )  {
        next()
    } else {
        const token = request.headers.token

        if (!token || token.length == 0) {
            response.send(utils.createResult('missing token'))
        } else {
            try {
                const payload = jwt.verify(token, config.secret)
                request.cid = payload.cid
                console.log(payload.cid)
                next()

            } catch (ex) {
                response.send(utils.createResult('Invalid token'))
            }

        }
    }
})

const adminSignup = require('./routes/admin')
app.use(adminSignup)
const adminSignin = require('./routes/admin')

app.use(adminSignin)
const services=require('./routes/services')
app.use(services)

const userSignup = require('./routes/user')
app.use(userSignup)
const userSignin = require('./routes/user')
app.use(userSignin)
const booking = require('./routes/booking')
app.use(booking)


app.listen(4000, '0.0.0.0', () => {
    console.log("server running...")
})